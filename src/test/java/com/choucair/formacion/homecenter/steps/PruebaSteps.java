package com.choucair.formacion.homecenter.steps;

import com.choucair.formacion.homecenter.pageobjects.PruebaPage;
import net.thucydides.core.annotations.Step;

public class PruebaSteps {
    private PruebaPage pruebaPage;

    @Step
    public void ingresarPagina(){
        pruebaPage.open();
        pruebaPage.frames();
    }
    @Step
    public void ingresarFormulario1() {
        pruebaPage.ingresarNombre();
        pruebaPage.ingresarApellido1();
        pruebaPage.ingresarApellido2();
    }
    @Step
    public void verificarVentajas(){
        pruebaPage.clickVentajas();
        pruebaPage.validarTextoVentajas();
    }
    @Step
    public void ingresarFormulario2() {
        pruebaPage.tipoDocumento();
        pruebaPage.ingresarDocumento();
        pruebaPage.enviarFormulario();

    }
    @Step
    public void verificarError() {
        pruebaPage.validarErrorEnviar();
    }


}
