package com.choucair.formacion.homecenter.definition;

import com.choucair.formacion.homecenter.steps.PruebaSteps;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class PruebaDefinition {

    @Steps
    private PruebaSteps pruebaSteps;

    @Given("^ingresar a la pagina$")
    public void ingresar_a_la_pagina(){
        pruebaSteps.ingresarPagina();
    }

    @When("^validar mensaje en la pestana ventajas y registrar$")
    public void validar_mensaje_en_la_pestana_ventajas_y_registrar(){
        pruebaSteps.ingresarFormulario1();
        pruebaSteps.verificarVentajas();
        pruebaSteps.ingresarFormulario2();
    }

    @Then("^mensaje de campos requeridos$")
    public void mensaje_de_campos_requeridos(){
        pruebaSteps.verificarError();
        }

    }

