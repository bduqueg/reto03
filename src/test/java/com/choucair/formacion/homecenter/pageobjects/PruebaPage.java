package com.choucair.formacion.homecenter.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;

@DefaultUrl("https://www.proteccion.com/wps/portal/proteccion/web/home/general/solicita-clave")
public class PruebaPage extends PageObject {

    @FindBy (xpath = "//*[@id=\'nombres\']")
    private WebElementFacade txtNombre;
    @FindBy(xpath = "//*[@id=\'primerApellido\']")
    private WebElementFacade txtApellido1;
    @FindBy(xpath = "//*[@id=\'segundoApellido\']")
    private WebElementFacade txtApellido2;
    @FindBy(xpath = "//*[@id=\'tab2\']")
    private WebElement btnVentajas;
    @FindBy (xpath = "//*[@id=\'contenido2\']/div/ul/li[3]")
    private WebElement lblAdministraVentajas;
    @FindBy (xpath = "//*[@id=\'tipoIdentificacion\']")
    private WebElement cmbDocumento;
    @FindBy (xpath = "//*[@id=\'tipoIdentificacion\']/option[1]")
    private WebElement cmbCedula;
    @FindBy (xpath = "//*[@id=\'identificacion\']")
    private WebElement txtDocuemento;
    @FindBy (xpath = "//*[@id=\'izquierdoAct\']/div[5]/a")
    private WebElement btnValidar;
    @FindBy (xpath = "//*[@id=\'popup_content\']")
    private WebElement lblError;

    public void frames(){
        getDriver().switchTo().frame("contenido");
        getDriver().switchTo().frame("contenido2");
    }

    public void ingresarNombre(){
        txtNombre.sendKeys("Prueba");
    }
    public void ingresarApellido1(){
        txtApellido1.click();
        txtApellido1.sendKeys("Pruebas1");
    }
    public void ingresarApellido2(){
        txtApellido2.click();
        txtApellido2.sendKeys("Pruebas2");
    }
    public void clickVentajas(){
        btnVentajas.click();
    }
    public void tipoDocumento(){
        cmbDocumento.click();
        cmbCedula.click();
    }
    public void ingresarDocumento(){
        txtDocuemento.click();
        txtDocuemento.sendKeys("1234567890");
    }
    public void enviarFormulario(){
        btnValidar.click();
    }
    public void validarTextoVentajas(){
        String txtMensaje = lblAdministraVentajas.getText();
        assertThat(txtMensaje,equalTo("Administre su cuenta individual de forma confidencial"));
    }
    public void validarErrorEnviar(){
        String txtMensaje = lblError.getText();
        assertThat(txtMensaje.substring(0,46),equalTo("- Debe ingresar una fecha de expedición valida"));
    }
}




